# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'
params = YAML.load_file('params.yaml')
attributes = YAML.load_file('attributes.yaml')

# use Vagrant API Version 2
Vagrant.configure("2") do |config|
  config.vm.box = "wheezy.box"
  config.vm.box_url = "http://79.172.49.99/wheezy.box"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end

  config.vm.define :mfc do |mfc|
    configure_vm(mfc, '192.168.33.141', attributes['mfc']['domain'], ['mfc'], params, attributes)
    mfc.vm.synced_folder params['folders']['mfc'], "/home/site/mfc", :mount_options => ["uid=1001,gid=1004"]
    mfc.vm.synced_folder params['folders']['ekburg'], "/home/site/ekburg", :mount_options => ["uid=1001,gid=1004"]
    mfc.vm.synced_folder params['folders']['reforma'], "/home/site/reforma", :mount_options => ["uid=1001,gid=1004"]
  end
end

def configure_vm(node, ip, hostname, recipes, params, attributes)
  node.vm.hostname = hostname
  node.vm.network :private_network, ip: ip # host machine appears as x.x.x.1
  node.vm.provision :chef_solo do |chef|
    chef.cookbooks_path   = File.dirname(__FILE__) + "/../../cookbooks"
    chef.roles_path       = File.dirname(__FILE__) + "/../../roles"
    chef.data_bags_path   = File.dirname(__FILE__) + "/../../data_bags"
    chef.json             = attributes
    if params['chef_debug']
      chef.log_level = "debug"
    end

    recipes.each { 
        |recipe|
        chef.add_recipe(recipe)
    }
  end
end
