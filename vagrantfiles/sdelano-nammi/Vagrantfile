# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'
params = YAML.load_file('params.yaml')
attributes = YAML.load_file('attributes.yaml')

# use Vagrant API Version 2
Vagrant.configure("2") do |config|
  config.vm.box = "wheezy.box"
  config.vm.box_url = "http://79.172.49.99/wheezy.box"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end

  config.vm.define :nammi do |nammi|
    configure_vm(nammi, '192.168.33.140', attributes['sdelano-nammi']['domain'], ['sdelano-nammi'])
    nammi.vm.synced_folder params['folders']['sdelano-nammi'], "/home/site/sdelano-nammi", :mount_options => ["uid=1001,gid=1004"]
  end
end

def configure_vm(node, ip, hostname, recipes)
  params = YAML.load_file('params.yaml')
  node.vm.hostname = hostname
  node.vm.network :private_network, ip: ip # host machine appears as x.x.x.1
  node.vm.provision :chef_solo do |chef|
    chef.cookbooks_path   = File.dirname(__FILE__) + "/../../cookbooks"
    chef.roles_path       = File.dirname(__FILE__) + "/../../roles"
    chef.data_bags_path   = File.dirname(__FILE__) + "/../../data_bags"
    chef.json             = YAML.load_file('attributes.yaml')
    if params['chef_debug']
      chef.log_level = "debug"
    end

    recipes.each { 
        |recipe|
        chef.add_recipe(recipe)
    }
  end
end
