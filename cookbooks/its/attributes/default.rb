default['its']['debug'] = 'true'
default['its']['user_authorized_keys'] = ['its']

default['its']['mysql']['db'] = 'its'
default['its']['mysql']['user'] = 'root'
default['its']['mysql']['password'] = ''
default['its']['mysql']['port'] = 3306
default['its']['mysql']['host'] = '127.0.0.1'

default['its']['appName'] = 'ITS'
default['its']['domain'] = 'its.local.ru'
default['its']['user'] = 'site'
default['its']['group'] = 'site'

default['its']['home_dir'] = '/home/site/'
default['its']['deploy_key'] = 'its'
