maintainer       'darkair'
maintainer_email 'darkair2@gmail.com'
license          "All rights reserved"
description      "Configures redsphere node"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"
depends          "apt"
depends          "ssh_known_hosts2"
depends          "services"
depends          "supervisor"
depends          "sudo"