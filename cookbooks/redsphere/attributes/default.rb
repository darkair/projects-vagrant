default['redsphere']['debug'] = 'true'
default['redsphere']['user_authorized_keys'] = ['redsphere']

default['redsphere']['mysql']['db'] = 'redsphere'
default['redsphere']['mysql']['user'] = 'root'
default['redsphere']['mysql']['password'] = ''
default['redsphere']['mysql']['port'] = 3306
default['redsphere']['mysql']['host'] = '127.0.0.1'

default['redsphere']['appName'] = 'RedSphere'
default['redsphere']['domain'] = 'redsphere.local.ru'
default['redsphere']['user'] = 'site'
default['redsphere']['group'] = 'site'

default['redsphere']['home_dir'] = '/home/site/'
default['redsphere']['deploy_key'] = 'redsphere'
