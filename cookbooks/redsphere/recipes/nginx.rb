include_recipe "services::packages_apt_source_66.ru_wheezy"

$home_dir = node['redsphere']['home_dir']
$project_dir = File.join($home_dir, "redsphere")

%w{nginx-common nginx-full}.each do |pkg|
  apt_preference pkg do
    pin "release o=66.ru"
    pin_priority "1001"
  end
end

package "nginx-full" do
  action [:install]
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
end

template '/etc/nginx/sites-available/redsphere' do
  source 'nginx.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
      {
          :domain => node['redsphere']['domain'],
          :project_dir => $project_dir,
          :project_name => 'redsphere'
      }
  )
  notifies :restart, 'service[nginx]'
end

link '/etc/nginx/sites-enabled/redsphere' do
  to '/etc/nginx/sites-available/redsphere'
end

service 'nginx' do
  action [:enable, :start]
end