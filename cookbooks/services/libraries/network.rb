module NetworkRoutines
  def get_ipaddresses()
    # Gets a list of local IP addresses from Ohai
    # * *Returns* :
    # - list of node's IP addresses

    require 'rubygems'
    require 'ohai'
 
    o = Ohai::System.new()
    o.all_plugins

    ipaddrs = []

    o['network']['interfaces'].each do |iface, addrs|
      if addrs['addresses'] then
        addrs['addresses'].each do |ip, params|
          ipaddrs << ip if params['family'].eql?('inet')
          ipaddrs << ip if params['family'].eql?('inet6')
        end
      end
    end

    return ipaddrs
  end
end
