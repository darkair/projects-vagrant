#!/bin/bash

USER=zabbix
PASSWORD=aegiph2O

LOGFILE=/var/log/zabbix-agent/mysqla.log

stime=$(date +%s)

MYSQL="/usr/bin/mysql -u$USER -p$PASSWORD"
MYSQLADMIN="/usr/bin/mysqladmin -u$USER -p$PASSWORD"

SLAVESTATUS=$(echo "show slave status\G" | $MYSQL )
STATUS=$($MYSQLADMIN status)


case $1 in
        uptime) echo "$STATUS" | cut -f2 -d":"|cut -f1 -d"T" ;;
        threads) echo "$STATUS" | cut -f3 -d":"|cut -f1 -d"Q" ;;
        questions) echo "$STATUS" | cut -f4 -d":"|cut -f1 -d"S" ;;
        slowqueries) echo "$STATUS" | cut -f5 -d":"|cut -f1 -d"O" ;;
        qps) echo "$STATUS" | cut -f9 -d":" ;;
        version) $MYSQL -V ;;
        ping) $MYSQLADMIN ping|grep alive|wc -l ;;
        replio) echo "$SLAVESTATUS" | awk '/Slave_IO_Running/ {if ($2=="Yes") print "1"; else print "0";}' ;;
        replsql) echo "$SLAVESTATUS" | awk '/Slave_SQL_Running/ {if ($2=="Yes") print "1"; else print "0";}' ;;
        repllag) echo "$SLAVESTATUS" | awk '/Seconds_Behind_Master/ { if ($2=="NULL") print "2678400"; else print $2}' ;;
	repllerrno) echo "$SLAVESTATUS" | awk '/Last_Errno:/ {print $2}' ;;
	aborted-clients) $MYSQLADMIN extended-status | awk '/Aborted_clients/ {print $4}' ;;
	aborted-connects) $MYSQLADMIN extended-status | awk '/Aborted_connects/ {print $4}' ;;
	bytes-sent) $MYSQLADMIN extended-status | awk '/Bytes_sent/ {print $4}' ;;
	connections) $MYSQLADMIN extended-status | awk '/Connections/ {print $4}' ;;
	idb-row-lock-time-avg) $MYSQLADMIN extended-status | awk '/Innodb_row_lock_time_avg/ {print $4}' ;;
	read_only) $MYSQLADMIN variables | awk '/read_only/ {if ($4=="ON") print "1"; else print "0";}' ;;
        *) $MYSQL -e "show status"  | awk "{if(\$1==\"$1\") print \$2}" ;;
esac

echo "$(date): $0 execution time = $(($(date +%s) - $stime)) seconds" >> $LOGFILE

exit 0
