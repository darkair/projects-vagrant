#!/bin/bash
# Get non-OK volume count in all LSI FusionMPT2 controllers

lspci | grep -q 'Fusion-MPT SAS-2' || exit 1

RAIDUTIL=/usr/sbin/sas2ircu

controllers=$($RAIDUTIL LIST | awk '$1 ~ /^[0-9]$/ {print $1}')

if [ x$controllers == x ]; then
        exit 1
fi

for controller in $controllers; do
        $RAIDUTIL $controller DISPLAY
done | \
awk 'BEGIN {fails = 0};
        /Status of volume/ {if ($5 != "Okay") fails += 1};
        END {print fails}'

exit 0

