#!/bin/bash

lspci | egrep -q 'Fusion-MPT SAS$|Fusion-MPT SAS ' || exit 1

RAIDUTIL=/usr/sbin/mpt-status

controllers=$($RAIDUTIL  -pq | cut -d'=' -f 2 | cut -d',' -f 1)

if [ x$controllers == x ]; then
        echo "No controllers found"
        exit 0
fi

total_fails=0

for controller in $controllers; do
        fails=$(mpt-status -i $controller -q | grep 'phy ' | grep -vc "state ONLINE");
        total_fails=$(($total_fails + $fails))
done

echo $total_fails

exit 0

