#!/bin/bash

MONGO="/usr/bin/mongo --quiet"

case $1 in
        -rs)    echo "rs.status().$2" | $MONGO | grep -v bye | sed -e 's/.*"\([0-9]*\)".*/\1/g';;
	-dbstats)   echo "db.stats().$2" | $MONGO | grep -v bye | sed -e 's/.*"\([0-9]*\)".*/\1/g';;
        *)      echo "db.serverStatus().$1" | $MONGO | grep -v bye | sed -e 's/.*"\([0-9]*\)".*/\1/g' ;;
esac

exit 0


