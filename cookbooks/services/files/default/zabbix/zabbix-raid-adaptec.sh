#!/bin/bash

RAIDUTIL=/opt/Adaptec/arcconf

lspci  | grep -q 'RAID.*Adaptec' || exit 1

controllers=$($RAIDUTIL GETVERSION | awk -F'#' '/Controller #/ {print $2}')

if [ x$controllers == x ]; then
        echo "No controllers found"
        exit 0
fi

for controller in $controllers; do
        echo -n "Controller $controller "
        $RAIDUTIL GETCONFIG $controller AD | \
        awk '/Controller Status/ {ct_status=$4};
                /Logical devices\/Failed\/Degraded/ {ld_status=$4};
                /Defunct disk drive count/ {defunct_hds=$6};
                END {print "status:",ct_status, ", LDs total/failed/degraded:", ld_status, ", defunct drives:", defunct_hds}'
done

exit 0
