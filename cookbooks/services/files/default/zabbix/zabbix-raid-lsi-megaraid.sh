#!/bin/bash

RAIDUTIL='/opt/MegaRAID/MegaCli/MegaCli64'

lspci | grep -qi 'LSI.*mega' || exit 1

$RAIDUTIL -PDList -aALL | \
    awk 'BEGIN {fails = 0};
        /Firmware state/ {if ($3 !~ /Online/) fails ++};
        END {print fails}'

exit 0
