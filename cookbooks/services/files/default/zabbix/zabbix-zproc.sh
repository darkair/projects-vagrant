#!/bin/bash

PROC=$1
TYPE=$2

case $TYPE in
        "size") ps -eo pid,size,cmd | awk "/$PROC/ {size=size+\$2}; END {print size}";;
        "rss")  ps -eo pid,rss,cmd | awk "/$PROC/ {rss=rss+\$2}; END {print rss}";;
        "uptime")       stime=$(ps -eo lstart,cmd | awk "\$6 ~ /$PROC/ {print \$1,\$2,\$3,\$4,\$5}" | while read date; do echo $(date -d"$date" +%s); done | sort -n | tail -1)
                        echo $(($(date +%s) - $stime));;
        *) echo "0";;
esac

exit 0

