#!/bin/bash

set -e

NFSSTAT="/usr/sbin/nfsstat -cl"

[ "$1" == "" ] && { echo 0; exit 0; }

$NFSSTAT | awk "/$1:/ {sum+=\$5}; END {printf \"%.0f\n\", sum}" || { echo 0; exit 0; }

exit 0

