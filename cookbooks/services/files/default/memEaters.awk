# File is managed by Chef. Do not edit.
# This program takes ps -eo pid,rss,lstart,args as input
# and outputs programs with RSS > ARGV[1] MiB
BEGIN {
    rssThreshold = rssThresholdMB*1024
}

$2 ~ /[0-9]+/ {
    if ($2 > rssThreshold) {
        pid = $1
        rssmb = $2/1024
        stime = $6 " " $4 " " $5 " " $7;
        cmd = $8

        for (args=9; args < NF; args ++) {
            cmd = cmd " " $args
        }

	printf("%.2f MB %s (pid: %d, started %s)\n", rssmb, cmd, pid, stime)
    }
}
