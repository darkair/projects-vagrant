#
# Cookbook Name:: services
# Recipe:: packages_apt_settings_prefer_squeeze
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

file "/etc/apt/apt.conf.d/00-prefer-stable" do
	action :delete
end

file "/etc/apt/apt.conf.d/00-prefer-squeeze" do
	owner "root"
	group "root"
	mode "0644"
	content "APT::Default-Release \"squeeze\";\n"
end

