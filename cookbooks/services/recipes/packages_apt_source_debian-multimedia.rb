#
# Cookbook Name:: services
# Recipe:: packages_apt_source_debian-multimedia
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "debian-multimedia" do
	uri "http://www.deb-multimedia.org"
	distribution node['lsb']['codename']
	components ["main", "non-free"]
end

package "deb-multimedia-keyring" do
	action [:install]
	options "--force-yes"
end

