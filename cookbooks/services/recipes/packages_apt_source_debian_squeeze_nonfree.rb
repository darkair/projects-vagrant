#
# Cookbook Name:: services
# Recipe:: packages_apt_source_debian_squeeze_nonfree
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

file "/etc/apt/sources.list.d/debian_stable_nonfree-source.list" do
	action :delete
end

apt_repository "debian_squeeze_nonfree" do
	uri "http://http.us.debian.org/debian/" 
	distribution "squeeze"
	components ["non-free"]
end

