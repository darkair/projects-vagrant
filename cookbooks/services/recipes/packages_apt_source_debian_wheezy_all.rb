#
# Cookbook Name:: services
# Recipe:: packages_apt_source_debian_wheezy_all
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "debian_wheezy_all" do
	uri "http://http.us.debian.org/debian/" 
	distribution "wheezy"
	components ["main","contrib","non-free"]
end

