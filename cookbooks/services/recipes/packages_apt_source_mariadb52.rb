#
# Cookbook Name:: services
# Recipe:: packages_apt_source_mariadb52
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "mariadb52" do
	uri "http://mirrors.fe.up.pt/pub/mariadb/repo/5.2/debian"
	distribution node['lsb']['codename']
	components ["main"]
	keyserver "keyserver.ubuntu.com"
	key "1BB943DB"
end

