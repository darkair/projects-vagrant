#
# Cookbook Name:: services
# Recipe:: packages_apt_source_dotdeb
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Dotdeb repo for squeeze uses PHP 5.3
# See http://www.dotdeb.org/instructions/

apt_repository "dotdeb" do
    uri "http://packages.dotdeb.org"
    distribution node['lsb']['codename']
    components ["all"]
    key "http://www.dotdeb.org/dotdeb.gpg"
    retries 3
end

