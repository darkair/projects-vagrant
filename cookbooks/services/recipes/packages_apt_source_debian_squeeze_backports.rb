#
# Cookbook Name:: services
# Recipe:: packages_apt_source_debian_squeeze_backports
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "debian_squeeze_backports" do
	uri "http://backports.debian.org/debian-backports" 
	distribution "squeeze-backports"
	components ["main"]
end

