#
# Cookbook Name:: services
# Recipe:: packages_apt_source_newrelic
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# Add newrelic APT repo
apt_repository "newrelic" do
    uri "http://apt.newrelic.com/debian/"
    distribution "newrelic"
    components ["non-free"]
    key "https://download.newrelic.com/548C16BF.gpg"
end

