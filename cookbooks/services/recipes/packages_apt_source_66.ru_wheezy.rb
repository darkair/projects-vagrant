# Add 66.ru APT repo
apt_repository "66.ru-wheezy" do
    uri "http://79.172.49.99/debian"
    distribution "wheezy"
    components ["main"]
    key "http://79.172.49.99/66.ru.gpg"
end
