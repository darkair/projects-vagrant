#
# Cookbook Name:: services
# Recipe:: packages_apt_source_10gen
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "10gen" do
	uri "http://downloads-distro.mongodb.org/repo/debian-sysvinit"
	distribution "dist"
	components ["10gen"]
	keyserver "keyserver.ubuntu.com"
	key "7F0CEB10"
end

