#
# Cookbook Name:: services
# Recipe:: packages_apt_source_percona
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "percona" do
    uri "http://repo.percona.com/apt"
    distribution node['lsb']['codename']
    components ["main"]
    key "http://79.172.49.99/percona.gpg"
    retries 3
end

