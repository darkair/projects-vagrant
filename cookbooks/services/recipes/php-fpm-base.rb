#
# Cookbook Name:: services
# Recipe:: php-fpm-base
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "services::packages_apt_source_dotdeb"

file "/etc/apt/preferences.d/php-from-dotdeb" do
    owner 'root'
    group 'root'
    mode '644'
    action :create
    content <<-'EOH'
Package: php*
Pin: release o=packages.dotdeb.org
Pin-priority: 1001
EOH
end


include_recipe "services::php-base"

%w{php5-fpm}.each do |pkg|
        package pkg do
                action :install
        end
end

service "php5-fpm" do
	action [:enable, :start]
end

