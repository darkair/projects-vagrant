#
# Cookbook Name:: services
# Recipe:: packages_apt_source_hwraid
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "hwraid" do
	uri "http://hwraid.le-vert.net/debian/"
	distribution node['lsb']['codename']
	components ["main"]
end

