#
# Cookbook Name:: services
# Recipe:: packages_apt_source_66.ru
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# Add 66.ru APT repo
apt_repository "66.ru" do
    uri "http://79.172.49.99/debian"
    distribution "stable"
    components ["main"]
    key "http://79.172.49.99/66.ru.gpg"
    retries 3
end

