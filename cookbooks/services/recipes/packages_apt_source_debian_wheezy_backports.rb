#
# Cookbook Name:: services
# Recipe:: packages_apt_source_debian_wheezy_backports
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "debian_wheezy_backports" do
    uri "http://http.us.debian.org/debian/" 
    distribution "wheezy-backports"
    components ["main"]
end

