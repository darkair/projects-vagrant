#
# Cookbook Name:: services
# Recipe:: packages_apt_source_debian_sid_all
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_repository "debian_sid_all" do
	uri "http://http.us.debian.org/debian/" 
	distribution "sid"
	components ["main","contrib","non-free"]
end

