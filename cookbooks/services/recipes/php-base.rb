#
# Cookbook Name:: services
# Recipe:: php-base
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "services::packages_apt_source_dotdeb"

%w{php5-cli php5-cgi php5-mysql php5-curl}.each do |pkg|
        package pkg do
                action :install
        end
end

