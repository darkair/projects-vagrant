include_recipe 'ddve::nginx'
include_recipe "services::php-fpm-base"
include_recipe 'ddve::common'
include_recipe 'ddve::project'
