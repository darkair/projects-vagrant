include_recipe "services::packages_apt_source_66.ru_wheezy"

$home_dir = node['ddve']['home_dir']
$project_dir = File.join($home_dir, "ddve")

%w{nginx-common nginx-full}.each do |pkg|
  apt_preference pkg do
    pin "release o=66.ru"
    pin_priority "1001"
  end
end

package "nginx-full" do
  action [:install]
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
end

template '/etc/nginx/sites-available/ddve' do
  source 'nginx.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
      {
          :domain => node['ddve']['domain'],
          :project_dir => $project_dir,
          :project_name => 'ddve'
      }
  )
  notifies :restart, 'service[nginx]'
end

link '/etc/nginx/sites-enabled/ddve' do
  to '/etc/nginx/sites-available/ddve'
end

service 'nginx' do
  action [:enable, :start]
end