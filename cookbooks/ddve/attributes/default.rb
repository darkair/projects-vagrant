default['ddve']['debug'] = 'true'
default['ddve']['user_authorized_keys'] = ['ddve']

default['ddve']['mysql']['db'] = 'ddve'
default['ddve']['mysql']['user'] = 'root'
default['ddve']['mysql']['password'] = ''
default['ddve']['mysql']['port'] = 3306
default['ddve']['mysql']['host'] = '127.0.0.1'

default['ddve']['appName'] = 'ddve'
default['ddve']['domain'] = 'ddve.local.ru'
default['ddve']['user'] = 'site'
default['ddve']['group'] = 'site'

default['ddve']['home_dir'] = '/home/site/'
default['ddve']['deploy_key'] = 'ddve'
