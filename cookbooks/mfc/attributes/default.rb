default['mfc']['debug'] = 'true'
default['mfc']['user_authorized_keys'] = ['mfc']

default['mfc']['mysql']['db'] = 'mfc'
default['mfc']['mysql']['user'] = 'root'
default['mfc']['mysql']['password'] = ''
default['mfc']['mysql']['port'] = 3306
default['mfc']['mysql']['host'] = '127.0.0.1'

default['mfc']['appName'] = 'MFC'
default['mfc']['domain'] = 'mfc.local.ru'
default['mfc']['user'] = 'site'
default['mfc']['group'] = 'site'

default['mfc']['home_dir'] = '/home/site/'
default['mfc']['deploy_key'] = 'mfc'

