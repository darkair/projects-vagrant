php_path = "/usr/bin/php"

git $project_dir do
    repository 'git@bitbucket.org:darkair/mfc.git'
    user $user
    group $group
    enable_submodules true
    depth 100
    action :sync
    revision node['mfc']['revision']
end

#composer "install composer" do
#    action [:install, :update]
#    home $home_dir
#    owner "site"
#    group "site"
#end

#execute "install global dev" do
#    cwd $home_dir
#    command 'COMPOSER_HOME=/home/site/.composer bin/composer.phar selfupdate;
#             COMPOSER_HOME=/home/site/.composer bin/composer.phar global require "fxp/composer-asset-plugin:1.0.*@dev"'
#    action :run
#    group "site"
#    user "site"
#end

#composer_package $project_dir do
#    action [:install, :update]
#    home $home_dir
#    user "site"
#    group "site"
#     dev true
#end

include_recipe 'services::packages_apt_source_debian_wheezy_backports'
include_recipe 'services::packages_apt_source_66.ru_wheezy'

directory File.join($project_dir, 'www', 'assets') do
  owner $user
  group $group
  mode '0755'
  action :create
end

directory File.join($project_dir, 'protected', 'runtime') do
  owner $user
  group $group
  mode '0755'
  action :create
end

template File.join($project_dir, "protected/config/params.php") do
    source "params.erb"
    owner $user
    group $group
    mode "0644"
    variables({
        :appName    => node['mfc']['appName'],
        :dbHost     => node['mfc']['mysql']['host'],
        :dbName     => node['mfc']['mysql']['db'],
        :dbUser     => node['mfc']['mysql']['user'],
        :dbPass     => node['mfc']['mysql']['password'],
        :debug      => node['mfc']['debug']
    })
end

# Migrate database
script "database migrate" do
    interpreter "bash"
    user "site"
    group "site"
    cwd File.join($project_dir, "protected")
    code php_path + " yiic migrate up 5 --interactive=0"
end

script "create auth user" do
    interpreter "bash"
    user "site"
    group "site"
    cwd File.join($project_dir, "protected")
    code php_path + " yiic createauthitems index --email=darkair2@gmail.com --password=darkair"
end
