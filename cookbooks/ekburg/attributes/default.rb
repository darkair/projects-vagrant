default['ekburg']['debug'] = 'true'
default['ekburg']['user_authorized_keys'] = ['mfc']

default['ekburg']['mysql']['db'] = 'ekburg'
default['ekburg']['mysql']['user'] = 'root'
default['ekburg']['mysql']['password'] = ''
default['ekburg']['mysql']['port'] = 3306
default['ekburg']['mysql']['host'] = '127.0.0.1'

default['ekburg']['appName'] = 'ekburg'
default['ekburg']['domain'] = 'ekburg.local.ru'
default['ekburg']['user'] = 'site'
default['ekburg']['group'] = 'site'

default['ekburg']['home_dir'] = '/home/site/'
default['ekburg']['deploy_key'] = 'mfc'

