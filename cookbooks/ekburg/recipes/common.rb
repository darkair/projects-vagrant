packages = [ "memcached", "git-core", "php5-memcached", "php5-mcrypt", "php5-gd", "imagemagick", "mysql-server" ]
php_conf = "/etc/php5/fpm/pool.d/www.conf"

$user         = node['ekburg']['user']
$group        = node['ekburg']['group']
$home_dir     = node['ekburg']['home_dir']
$project_dir  = File.join($home_dir, 'ekburg')
ssh_dir       = File.join($home_dir, '.ssh')


execute "apt-get-update" do
  command "apt-get update"
  ignore_failure true
  action :run
end

package "mc"

packages.each do |pkg|
    package pkg do
        action :install
        options "--allow-unauthenticated"
        notifies :restart, "service[php5-fpm]"
    end
end

# install mysql-client if not exists
# for example, if you have mysql-server and mysql-client both in one place
package "mysql-client-5.6" do
	action :install
	not_if "dpkg -l | grep -q mariadb-client"
end

group $group

user $user do
  gid $group
  home $home_dir
  shell '/bin/bash'
end

directory $home_dir do
  owner $user
  group $group
  mode '0755'
  action :create
end

directory ssh_dir do
  owner $user
  group $group
  mode '0700'
  action :create
end


pubkey_list = []
node['ekburg']['user_authorized_keys'].each do |auth_user|
  search(:users, "id:#{ auth_user }") do |u|
    pubkeys = u['pubkeys']
    pubkeys.each do |pubkey|
      pubkey_list << pubkey
    end
  end
end

template File.join(ssh_dir, 'authorized_keys') do
  source 'authorized_keys.erb'
  owner $user
  group $group
  mode '0600'
  variables :ssh_keys => pubkey_list
end

deploy_key = node['ekburg']['deploy_key']
private_key = search(:deploy_keys, "id:#{ deploy_key }")[0]['private_key']

file File.join(ssh_dir, 'id_rsa') do
  owner $user
  group $group
  mode '0600'
  action :create_if_missing
  content private_key
end

include_recipe 'services::packages_apt_source_66.ru'

#known_hosts_file = File.join(ssh_dir, 'known_hosts')

#ssh_known_hosts2_edit known_hosts_file do
#  host 'bitbucket.org,207.223.240.182'
#  key 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw=='
#  filename known_hosts_file
#  owner $user
#  group $group
#  action :modify
#end


# Add github to known_hosts
known_hosts = "|1|QWu7NcqiX87pjyt+246eNGi5Zj4=|6cSXSMPIZCTfaE6hboO4zIUiXFU= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==\n|1|EHmPQEjFUzg87JL312ZI4SoyJk8=|MmX4/QGu0ve2HvK2RWWsORi2bIA= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==\n"

file File.join($home_dir, '.ssh/known_hosts') do
  owner $user
  group $group
  mode "0600"
  action :create
  content known_hosts
end

directory "/var/log/php5-fpm" do
    action :create
    mode "0755"
    notifies :restart, "service[php5-fpm]"
end

template php_conf do
    source "php-fpm.erb"
    action :create
    owner "root"
    group "root"
    mode "0644"
    variables({
        :project_dir => $project_dir,
        :error_log => node['ekburg']['domain'] + '.phperrors.log'
    })
    notifies :restart, "service[php5-fpm]"
end

sudo 'site' do
  user '%site'
  runas 'root'
  commands [
               '/usr/local/bin/supervisorctl',
               '/usr/bin/chef-client',
               '/etc/init.d/chef-client',
           ]
  nopasswd true
end

%w{/var/log/syslog /var/log/nginx/error.log /var/log/nginx/access.log}.each do |file|
  bash "chmod #{ file }" do
    code "[[ -e #{ file } ]] && chmod 0644 #{ file } || exit 0"
  end
end