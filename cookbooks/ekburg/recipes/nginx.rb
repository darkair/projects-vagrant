include_recipe "services::packages_apt_source_66.ru_wheezy"

$home_dir = node['ekburg']['home_dir']
$project_dir = File.join($home_dir, "ekburg")

%w{nginx-common nginx-full}.each do |pkg|
  apt_preference pkg do
    pin "release o=66.ru"
    pin_priority "1001"
  end
end

package "nginx-full" do
  action [:install]
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
end

template '/etc/nginx/sites-available/ekburg' do
  source 'nginx.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
      {
          :domain => node['ekburg']['domain'],
          :project_dir => $project_dir,
          :project_name => 'ekburg'
      }
  )
  notifies :restart, 'service[nginx]'
end

link '/etc/nginx/sites-enabled/ekburg' do
  to '/etc/nginx/sites-available/ekburg'
end

service 'nginx' do
  action [:enable, :start]
end